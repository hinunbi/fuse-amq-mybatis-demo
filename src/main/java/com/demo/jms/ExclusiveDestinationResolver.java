package com.demo.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

import org.springframework.jms.support.destination.DestinationResolver;
import org.springframework.stereotype.Component;


@Component
public class ExclusiveDestinationResolver implements DestinationResolver {

  String destinationOptions = "consumer.exclusive=true";

  @Override
  public Destination resolveDestinationName(Session session, String destinationName, boolean pubSubDomain)
      throws JMSException {
    if (pubSubDomain) {
      return session.createTopic(destinationName);
    } else {
      if (destinationName.indexOf('?') > -1) {
        return session.createQueue(destinationName + "&" + destinationOptions);
      } else {
        return session.createQueue(destinationName + "?" + destinationOptions);
      }
    }
  }
}