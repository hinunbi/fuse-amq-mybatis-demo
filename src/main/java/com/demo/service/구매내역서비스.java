package com.demo.service;

import com.demo.infra.repository.RepositorySupport;
import com.demo.model.구매내역;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class 구매내역서비스 extends RepositorySupport {

  @Transactional("databaseTxManager")
  public int process(구매내역 in) {

    in.set구매번호(Long.toString(RandomUtils.nextLong()));

    int result = getSqlSession().insert(
        "mappers.구매내역Mapper.insert", in);
    return result;
  }
}
